#pragma once

#include "stdafx.h"

class ProfilingArchiver
{
public:

   static void ZipFiles(const QString& sourceDir, const QString& ext, const QString& outFile);
};
