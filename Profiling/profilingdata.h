#pragma once

#include "stdafx.h"

struct ProfilingData
{
   QString
      FunctionName;

   std::size_t
      Begin,
      End,
      thread_id;

   bool operator==(const ProfilingData& o) const noexcept
   {
      return(FunctionName  == o.FunctionName &&
             Begin         == o.Begin        &&
             End           == o.End          &&
             thread_id     == o.thread_id);
   }

   bool operator!=(const ProfilingData& o) const noexcept
   {
      return !operator==(o);
   }
};