#pragma once

#include "stdafx.h"
#include "profilingdata.h"
#include "analysisdata.h"

class Analyzer
{
public:

   using ResultData = std::map<QString, AnalysisData>;

   static void RegisterProfilingData(const ProfilingData& data);
   static const ResultData& GetResults();

private:

   static const ResultData& CalculateResults();
   static std::map<QString, std::vector<ProfilingData>> ANALYSIS_REGISTER;
   static std::map<QString, AnalysisData> ANALYSIS_RESULT;
   static std::mutex MUTEX;
   static bool DIRTY;

};
