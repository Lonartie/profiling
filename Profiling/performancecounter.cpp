#include "stdafx.h"
#include "performancecounter.h"

#include <iostream>

using namespace HardwareAdapter;

ComputerObject PerformanceCounter::computer;
std::chrono::duration<long long, std::nano> PerformanceCounter::period;
std::chrono::time_point<std::chrono::steady_clock> PerformanceCounter::begin = std::chrono::high_resolution_clock::now();
std::thread PerformanceCounter::worker;
volatile bool PerformanceCounter::stopped = true;
std::map<unsigned, QFile> PerformanceCounter::files;

namespace
{
   std::vector<IHardwareObject> GetSubHardware(const IHardwareObject& hardware)
   {
      std::vector<IHardwareObject> result;

      for (const auto& subhardware : hardware.GetSubHardware())
      {
         result.push_back(subhardware);
         for (const auto& subsubhardware : GetSubHardware(hardware))
         {
            result.push_back(subsubhardware);
         }
      }

      return result;
   }
}

void PerformanceCounter::Stop()
{
   stopped = true;
   if (worker.joinable())
      worker.join();
}

std::map<IHardwareObject, std::vector<ISensorObject>> __cache__;

std::map<IHardwareObject, std::vector<ISensorObject>> PerformanceCounter::get_data()
{
   if (__cache__.empty())
   {
      std::map<IHardwareObject, std::vector<ISensorObject>> result;

      for (const auto& hardware : computer.GetHardware())
      {
         hardware.Update();
         result.emplace(hardware, std::vector<ISensorObject>{});
         for (const auto& sensor : hardware.GetSensors())
         {
            result[hardware].push_back(sensor);
         }

         for (const auto& shardware : GetSubHardware(hardware))
         {
            shardware.Update();
            result.emplace(shardware, std::vector<ISensorObject>{});
            for (const auto& sensor : shardware.GetSensors())
            {
               result[shardware].push_back(sensor);
            }
         }
      }
      __cache__ = result;
      return result;
   }
   else
   {
      for (const auto& kvp : __cache__)
      {
         kvp.first.Update();
      }

      return __cache__;
   }
}

std::chrono::duration<long long, std::nano> PerformanceCounter::measure(std::function<void()> fn)
{
   auto begin = std::chrono::high_resolution_clock::now();
   fn();
   auto end = std::chrono::high_resolution_clock::now();
   return std::chrono::duration_cast<std::chrono::duration<long long, std::nano>>(end - begin);
}

void PerformanceCounter::work()
{
   static decltype(period) last_duration = decltype(period)(0);
   while (!stopped)
   {
      last_duration = measure([]()
      {
         write_node();
      });

      // ensure we really tick in period steps! (because waiting for period time + function is more than period time)
      auto time = period - last_duration;

      if (time.count() > 0)
         std::this_thread::sleep_for(time);
   }
   write_end();
}

void PerformanceCounter::write_begin()
{
   int index = 0;
   for (const auto& data : get_data())
   {
      const auto& hardware = data.first;
      const auto& sensors = data.second;
      const auto base_file = QString("%1_%2_%3")
         .arg(HardwareTypeToString(hardware.GetHardwareType()))
         .arg(hardware.GetName())
         .arg(index)
         .replace(" ", "_");

      files.emplace(index, base_file + ".log");
      auto& file = files[index];
      file.open(QIODevice::WriteOnly);

      file.write("\"time\";");
      for (const auto& sensor : sensors)
      {
         file.write(QString("\"%1_%2\";")
                    .arg(SensorTypeToString(sensor.GetSensorType()))
                    .arg(sensor.GetName())
                    .toLatin1());

         for (const auto& param : sensor.GetParameters())
         {
            file.write(QString("\"%1_%2_%3\";")
                       .arg(SensorTypeToString(sensor.GetSensorType()))
                       .arg(sensor.GetName())
                       .arg(param.GetName())
                       .toLatin1());
         }
      }

      file.write("\n");

      QFile report(base_file + "_report.log");
      report.open(QIODevice::WriteOnly);
      report.write(hardware.GetReport().toLatin1());
      report.flush();
      report.close();
      index++;
   }
}

void PerformanceCounter::write_node()
{
   int index = 0;
   for (const auto& data : get_data())
   {
      const auto& hardware = data.first;
      const auto& sensors = data.second;
      auto report = hardware.GetReport();
      auto& file = files[index];
      auto time = std::chrono::high_resolution_clock::now();
      auto tif = std::chrono::duration_cast<std::chrono::nanoseconds>(time - begin);
      auto now = tif.count();

      file.write(QString("%1;")
                 .arg(now)
                 .toLatin1());

      for (const auto& sensor : sensors)
      {
         file.write(QString("%1;")
                    .arg(sensor.GetValue())
                    .toLatin1());

         for (const auto& param : sensor.GetParameters())
            file.write(QString("%1;")
                       .arg(param.GetValue())
                       .toLatin1());
      }

      file.write("\n");
      index++;
   }
}

void PerformanceCounter::write_end()
{
   for (auto& kvp : files)
   {
      kvp.second.flush();
      kvp.second.close();
   }

   std::cout << "running direct x diagnostics\n";

   // write dxdiag output


   system("dxdiag /whql:off /dontskip /x dxdiag_run.log");

   auto b1 = std::chrono::system_clock::now();
   while (!QFile::exists("./dxdiag_run.log") &&
          std::chrono::duration_cast<std::chrono::seconds>(
             std::chrono::system_clock::now() - b1)
          .count() < 5)
      std::this_thread::sleep_for(std::chrono::milliseconds(100));


   std::cout << "running direct x information\n";

   auto b2 = std::chrono::system_clock::now();
   system("dxdiag /whql:off /dontskip /t dxdiag_info.log");
   while (!QFile::exists("./dxdiag_info.log") &&
          std::chrono::duration_cast<std::chrono::seconds>(
             std::chrono::system_clock::now() - b2)
          .count() < 5)
      std::this_thread::sleep_for(std::chrono::milliseconds(100));

}
