#include "stdafx.h"
#include "profilingarchiver.h"
#include "../HardwareAdapter/Zipper.h"

using namespace HardwareAdapter;

void ProfilingArchiver::ZipFiles(const QString& sourceDir, const QString& ext, const QString& outFile)
{
   QDirIterator it(sourceDir, QStringList() << ext);

   if (QDir("__temp__").exists())
      QDir("__temp__").removeRecursively();

   if (QFile("./dump.dump").exists())
      QFile("./dump.dump").remove();

   QDir().mkdir("./__temp__/");

   while(it.hasNext())
   {
      QFileInfo info(it.next());
      if(QFile::copy(info.absoluteFilePath(), QDir("__temp__").absolutePath() + "/" + info.fileName()))
         QFile::remove(info.absoluteFilePath());
   }

   Zipper::ZipFolder("__temp__", outFile);
   QDir("__temp__").removeRecursively();
}
