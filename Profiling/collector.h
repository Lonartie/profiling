#pragma once

#include "stdafx.h"
#include "ProfilingData.h"

class Collector
{
public:

   static void Init();
   static void Close();
   static void WriteBegin(const ProfilingData& data);
   static void WriteEnd(const ProfilingData& data);

private:

   static void check_for_write();

   static QFile
      current_trace_file;

   static std::size_t
      trace_number;

   static QString
      trace_file_base_name,
      temp_content;

   static bool
      first_written,
      stopped;
};
