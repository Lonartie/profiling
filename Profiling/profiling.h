#pragma once

#include "stdafx.h"
#include "profiling_macros.h"
#include "performancecounter.h"
#include "collector.h"
#include "terminationhandler.h"
#include "profilingarchiver.h"

class Profiling
{
public:

   static QString GetModuleName();
   static bool IsElevated();

   using nanoseconds = std::chrono::nanoseconds;
   using microseconds = std::chrono::microseconds;
   using milliseconds = std::chrono::milliseconds;
   using seconds = std::chrono::seconds;
   using minutes = std::chrono::minutes;
   using hours = std::chrono::hours;

   static void SetCurrent(Profiling* pr);
   static void DelCurrent();
   static Profiling* GetCurrent();

   static void SetEnabled(bool enabled);
   static bool IsEnabled();

   Profiling(const char* name);
   Profiling(const char* name, std::size_t max_tracking_count);
   Profiling(bool dummy = true);
   ~Profiling();

   void start();
   void stop();
   void save();
   void destruct();

   template<typename RESOLUTION>
   long long elapsed() const;

private:

   static std::map<std::thread::id, std::vector<Profiling*>> CURRENT;
   static bool enabled;
   static Profiling* DUMMY;
   static std::chrono::steady_clock::time_point APPSTART;
   static std::mutex MUTEX;
   static std::map<QString, std::size_t> TRACK_REGISTER;

   long long result() const;

   bool m_deleted = false, dum = false;
   const char* func;
   std::chrono::nanoseconds begin, end;
};

template<typename RESOLUTION>
long long Profiling::elapsed() const
{
   auto el = std::chrono::high_resolution_clock::now() - begin;
   return std::chrono::duration_cast<RESOLUTION>(el).count();
}
