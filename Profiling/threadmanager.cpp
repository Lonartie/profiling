#include "stdafx.h"
#include "threadmanager.h"
#include <iostream>

std::deque<std::function<void()>> ThreadManager::POOL;
//std::vector<std::function<void()>> ThreadManager::POOL;
std::thread ThreadManager::WORKER;
std::mutex ThreadManager::MUTEX;
bool ThreadManager::STOPPED = false;
bool ThreadManager::STARTED = false;

void ThreadManager::Insert(std::function<void()> func)
{
   add(func);
}

void ThreadManager::Start()
{
   QFile readme("./readme.log");
   readme.open(QIODevice::WriteOnly);
   readme.write("This dump was created by profiling api � by L�on Gierschner\n\n");
   readme.write("dump.log     --> chrome://tracing                               (tracing)\n");
   readme.write("*_report.log --> notepad                                        (information)\n");
   readme.write("*.log        --> DatPlot.exe [http://www.datplot.com/download/] (resource usage)\n");
   readme.flush();
   readme.close();

   WORKER = std::thread(&ThreadManager::worker_func);
   STARTED = true;
}

void ThreadManager::Stop()
{
   STOPPED = true;

   if (WORKER.joinable())
      WORKER.join();
}

bool ThreadManager::Started()
{
   return STARTED;
}

void ThreadManager::worker_func()
{
   while (true)
   {
      static bool printed = false;
      if (!printed && STOPPED)
      {
         printed = true;
         std::cout << "waiting for dump file to be written...\n";
      }

      if (empty())
      {
         if (STOPPED) return;

         std::this_thread::sleep_for(std::chrono::milliseconds(100));
         continue;
      }

      first()();
      //if (POOL.size() % 10000 == 0)
      //   std::cout << POOL.size() << std::endl;
   }
}

void ThreadManager::add(std::function<void()> func)
{
   MUTEX.lock();
   POOL.push_back(func);
   MUTEX.unlock();
}

bool ThreadManager::empty()
{
   bool result = true;
   MUTEX.lock();
   result = POOL.empty();
   MUTEX.unlock();
   return result;
}

std::function<void()> ThreadManager::first()
{
   std::function<void()> result = nullptr;
   MUTEX.lock();

   if (!POOL.empty())
   {
      result = POOL.front();
      POOL.pop_front();
      //POOL.erase(POOL.begin());
   }

   MUTEX.unlock();
   return result;
}

