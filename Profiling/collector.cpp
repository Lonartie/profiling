#include "stdafx.h"
#include "collector.h"
#include "threadmanager.h"

#include <iostream>

#define WRITE_DIRECT

QFile Collector::current_trace_file;
std::size_t Collector::trace_number = 0;
QString Collector::temp_content = "";
QString Collector::trace_file_base_name = "dump.log";
bool Collector::stopped = true;
bool Collector::first_written = false;

void Collector::Init()
{
   stopped = false;
   ThreadManager::Start();
   current_trace_file.setFileName(trace_file_base_name);
   current_trace_file.open(QIODevice::WriteOnly);
   current_trace_file.write("{ \n\t\"traceEvents\": \n\t[\n");
}

void Collector::Close()
{
   if (stopped) return;
   stopped = true;
   ThreadManager::Stop();
   current_trace_file.write(temp_content.toLatin1());
   temp_content.clear();
   current_trace_file.flush();
   current_trace_file.write("\t], \n\t\"displayTimeUnit\": \"ns\" \n");

   current_trace_file.write("}");
   current_trace_file.close();
}

void Collector::WriteBegin(const ProfilingData& data)
{
   static const QString templ("\t\t\t\"%1\": \"%2\"%3\n");

#ifdef WRITE_DIRECT
   if (first_written)
      current_trace_file.write("\t\t, {\n");
   else
      current_trace_file.write("\t\t{\n");

   current_trace_file.write(templ.arg("name").arg(data.FunctionName).arg(",").replace("\\", "\\\\").toLatin1());
   current_trace_file.write(templ.arg("cat").arg("perf").arg(",").toLatin1());
   current_trace_file.write(templ.arg("ph").arg("B").arg(",").toLatin1());
   current_trace_file.write(templ.arg("pid").arg(QCoreApplication::applicationPid()).arg(",").toLatin1());
   current_trace_file.write(templ.arg("ts").arg(data.Begin / 1000.0).arg(",").toLatin1());
   current_trace_file.write(templ.arg("tid").arg(data.thread_id).arg("").toLatin1());
   current_trace_file.write("\t\t}\n");
   check_for_write();
#else
   if (first_written)
      temp_content.append("\t\t, {\n");
   else
      temp_content.append("\t\t{\n");

   temp_content.append(templ.arg("name").arg(data.FunctionName).arg(",").replace("\\", "\\\\").toLatin1());
   temp_content.append(templ.arg("cat").arg("perf").arg(",").toLatin1());
   temp_content.append(templ.arg("ph").arg("B").arg(",").toLatin1());
   temp_content.append(templ.arg("pid").arg(QCoreApplication::applicationPid()).arg(",").toLatin1());
   temp_content.append(templ.arg("ts").arg(data.Begin / 1000.0).arg(",").toLatin1());
   temp_content.append(templ.arg("tid").arg(data.thread_id).arg("").toLatin1());
   temp_content.append("\t\t}\n");
   check_for_write();
#endif
   trace_number++;
   first_written = true;
}

void Collector::WriteEnd(const ProfilingData& data)
{
   static const QString templ("\t\t\t\"%1\": \"%2\"%3\n");

#ifdef WRITE_DIRECT
   if (first_written)
      current_trace_file.write("\t\t, {\n");
   else
      current_trace_file.write("\t\t{\n");

   current_trace_file.write(templ.arg("name").arg(data.FunctionName).arg(",").replace("\\", "\\\\").toLatin1());
   current_trace_file.write(templ.arg("cat").arg("perf").arg(",").toLatin1());
   current_trace_file.write(templ.arg("ph").arg("E").arg(",").toLatin1());
   current_trace_file.write(templ.arg("pid").arg(QCoreApplication::applicationPid()).arg(",").toLatin1());
   current_trace_file.write(templ.arg("ts").arg(data.End / 1000.0).arg(",").toLatin1());
   current_trace_file.write(templ.arg("tid").arg(data.thread_id).arg("").toLatin1());
   current_trace_file.write("\t\t}\n");
#else
   if (first_written)
      temp_content.append("\t\t, {\n");
   else
      temp_content.append("\t\t{\n");

   temp_content.append(templ.arg("name").arg(data.FunctionName).arg(",").replace("\\", "\\\\").toLatin1());
   temp_content.append(templ.arg("cat").arg("perf").arg(",").toLatin1());
   temp_content.append(templ.arg("ph").arg("E").arg(",").toLatin1());
   temp_content.append(templ.arg("pid").arg(QCoreApplication::applicationPid()).arg(",").toLatin1());
   temp_content.append(templ.arg("ts").arg(data.End / 1000.0).arg(",").toLatin1());
   temp_content.append(templ.arg("tid").arg(data.thread_id).arg("").toLatin1());
   temp_content.append("\t\t}\n");
   check_for_write();
#endif
}

void Collector::check_for_write()
{
#ifndef WRITE_DIRECT
   constexpr std::size_t max_size = 1024 /*kb*/ * 1024 /*mb*/ * 50;
   if (static_cast<std::size_t>(temp_content.size()) >= max_size)
   {
      current_trace_file.write(temp_content.toLatin1());
      temp_content.clear();
      std::cout << "writing..." << std::endl;
   }
#endif
}