#pragma once

// Qt
#include <QtCore>

// Stl
#include <memory>
#include <vector>
#include <map>
#include <functional>
#include <string>
#include <array>
#include <cassert>
#include <math.h>
#include <algorithm>
#include <numeric>
#include <xutility>
#include <thread>
#include <chrono>
#include <deque>
