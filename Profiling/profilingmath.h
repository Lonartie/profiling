#pragma once

#include "stdafx.h"

class ProfilingMath
{
public:

   static double MeanWithHistogram(const std::vector<std::size_t>& list, double histogram);
   static double Mean(const std::vector<std::size_t>& list);
   static std::size_t Min(const std::vector<std::size_t>& list);
   static std::size_t Max(const std::vector<std::size_t>& list);

};
