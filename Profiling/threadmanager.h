#pragma once

#include "stdafx.h"

class ThreadManager
{
public:

   static void Insert(std::function<void()> func);
   static void Start();
   static void Stop();
   static bool Started();

private:

   static void worker_func();
   static void add(std::function<void()> func);
   static bool empty();
   static std::function<void()> first();

   static std::deque<std::function<void()>> POOL;
   //static std::vector<std::function<void()>> POOL;
   static std::thread WORKER;
   static std::mutex MUTEX;
   static bool STOPPED;
   static bool STARTED;
};
