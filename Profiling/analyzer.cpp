#include "stdafx.h"
#include "analyzer.h"
#include "profilingmath.h"

std::map<QString, std::vector<ProfilingData>> Analyzer::ANALYSIS_REGISTER = {};
std::map<QString, AnalysisData> Analyzer::ANALYSIS_RESULT = {};
std::mutex Analyzer::MUTEX;
bool Analyzer::DIRTY = false;

void Analyzer::RegisterProfilingData(const ProfilingData& data)
{
   MUTEX.lock();
   if (std::find_if(ANALYSIS_REGISTER.begin(), ANALYSIS_REGISTER.end(), [&](auto& kvp)
   { return kvp.first == data.FunctionName; }) == ANALYSIS_REGISTER.end())
      ANALYSIS_REGISTER.emplace(data.FunctionName, std::vector<ProfilingData> {});

   ANALYSIS_REGISTER[data.FunctionName].push_back(data);
   DIRTY = true;
   MUTEX.unlock();
}

const Analyzer::ResultData& Analyzer::GetResults()
{
   if (DIRTY)
      return CalculateResults();

   return ANALYSIS_RESULT;
}

const Analyzer::ResultData& Analyzer::CalculateResults()
{
   MUTEX.lock();
   ANALYSIS_RESULT.clear();

   for (const auto& func_data : ANALYSIS_REGISTER)
   {
      QString name = func_data.first;

      std::size_t
         min = std::numeric_limits<std::size_t>::max(),
         max = std::numeric_limits<std::size_t>::min(),
         tile = 0,
         count = func_data.second.size();

      double
         period_mean = 0,
         duration_mean = 0,
         duration_dif = 0,
         duration_stddev = 0,
         duration_stddev_sq_sum = 0,
         duration_stddev_mean = 0;

      std::vector<std::size_t>
         period_list,
         duration_list;

      // mean min max searching
      for (std::size_t i = 0; i < func_data.second.size(); i++)
      {
         auto& profiling_data = func_data.second[i];
         tile = profiling_data.End - profiling_data.Begin;
         duration_list.push_back(tile);

         if (profiling_data != func_data.second.end()[-1])
         {
            auto& profiling_data_next = func_data.second[i + 1];
            tile = (profiling_data_next.Begin - profiling_data.Begin);
            period_list.push_back(tile);
         }
      }

      min = ProfilingMath::Min(duration_list);
      max = ProfilingMath::Max(duration_list);
      duration_mean = ProfilingMath::MeanWithHistogram(duration_list, 0.90);
      period_mean = ProfilingMath::MeanWithHistogram(period_list, 0.90);

      // std deviation calculation
      for (const auto& profiling_data : func_data.second)
      {
         tile = profiling_data.End - profiling_data.Begin;
         duration_dif = tile - duration_mean;
         duration_stddev_sq_sum += duration_dif * duration_dif;
      }

      duration_stddev_mean = duration_stddev_sq_sum / static_cast<double>(count);
      duration_stddev = std::sqrt(duration_stddev_mean);


      AnalysisData result;
      result.function_name = name;
      result.period_mean = period_mean;
      result.duration_mean = duration_mean;
      result.duration_stddev = duration_stddev;
      result.duration_min = min;
      result.duration_max = max;

      ANALYSIS_RESULT[name] = result;
   }

   DIRTY = false;
   MUTEX.unlock();
   return ANALYSIS_RESULT;
}
