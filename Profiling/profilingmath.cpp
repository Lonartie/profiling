#include "stdafx.h"
#include "profilingmath.h"

double ProfilingMath::MeanWithHistogram(const std::vector<std::size_t>& list, double histogram)
{
   if (list.empty()) return 0;
   auto v = list;
   auto u = static_cast<std::size_t>(list.size() * histogram);
   std::nth_element(v.begin(),
                    v.begin() + u, 
                    v.end(),
                    std::less<std::size_t>());

   v.erase(v.begin() + u, v.end());
   return Mean(v);
}

double ProfilingMath::Mean(const std::vector<std::size_t>& list)
{
   if (list.empty()) return 0;
   auto v = list;
   std::nth_element(v.begin(), v.begin() + v.size() / 2, v.end());
   return v[v.size() / 2];
}

std::size_t ProfilingMath::Min(const std::vector<std::size_t>& list)
{
   if (list.empty()) return 0;
   auto v = list;
   std::nth_element(v.begin(), v.begin(), v.end(), std::less<std::size_t>());
   return v[0];
}

std::size_t ProfilingMath::Max(const std::vector<std::size_t>& list)
{
   if (list.empty()) return 0;
   auto v = list;
   std::nth_element(v.begin(), v.begin(), v.end(), std::greater<std::size_t>());
   return v[0];
}
