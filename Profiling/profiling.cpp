#include "stdafx.h"
#include "profiling.h"
#include "collector.h"
#include "threadmanager.h"
#include "analyzer.h"

#include <iostream>
#include <windows.h>

using namespace std::chrono;

std::map<std::thread::id, std::vector<Profiling*>> Profiling::CURRENT;

bool Profiling::enabled;
Profiling* Profiling::DUMMY = new Profiling(true);
steady_clock::time_point Profiling::APPSTART = high_resolution_clock::now();
std::mutex Profiling::MUTEX;
std::map<QString, std::size_t> Profiling::TRACK_REGISTER;

QString Profiling::GetModuleName()
{
   long long size = MAX_PATH;
   char buf[MAX_PATH];
   size = GetModuleFileNameA(NULL, buf, size);
   return QString::fromLatin1(buf, size);
}

bool Profiling::IsElevated()
{
   BOOL fRet = FALSE;
   HANDLE hToken = NULL;
   if (OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hToken))
   {
      TOKEN_ELEVATION Elevation;
      DWORD cbSize = sizeof(TOKEN_ELEVATION);
      if (GetTokenInformation(hToken, TokenElevation, &Elevation, sizeof(Elevation), &cbSize))
      {
         fRet = Elevation.TokenIsElevated;
      }
   }
   if (hToken)
   {
      CloseHandle(hToken);
   }
   return fRet;
}

void Profiling::SetCurrent(Profiling* pr)
{
   MUTEX.lock();
   auto tid = std::this_thread::get_id();

   bool contains = false;
   for (const auto& kvp : CURRENT)
      if (kvp.first == tid)
      {
         contains = true;
         break;
      }

   if (!contains)
      CURRENT.emplace(tid, std::vector<Profiling*>{});

   CURRENT[tid].push_back(pr);
   MUTEX.unlock();
}

void Profiling::DelCurrent()
{
   MUTEX.lock();
   if (!CURRENT.empty())
      CURRENT[std::this_thread::get_id()].resize(CURRENT.size() - 1);
   MUTEX.unlock();
}

Profiling* Profiling::GetCurrent()
{
   Profiling* result = nullptr;
   MUTEX.lock();
   if (!CURRENT.empty())
      result = CURRENT[std::this_thread::get_id()].back();
   MUTEX.unlock();
   return result;
}

void Profiling::SetEnabled(bool enabled)
{
   Profiling::enabled = enabled;
}

bool Profiling::IsEnabled()
{
   return Profiling::enabled;
}

Profiling::Profiling(const char* name)
   : func(name)
{
   if (!enabled) return;
   SetCurrent(this);
   start();
}

Profiling::Profiling(const char* name, std::size_t max_tracking_count)
   : func(name)
{
   if (!enabled) return;

   QString _name_tid = QString("%1_%2").arg(name).arg(std::hash<std::thread::id>{}(std::this_thread::get_id()));

   if (std::find_if(TRACK_REGISTER.begin(), TRACK_REGISTER.end(), [&](auto& kvp) { return kvp.first == _name_tid; }) == TRACK_REGISTER.end())
      TRACK_REGISTER.emplace(_name_tid, 0);

   if (TRACK_REGISTER[_name_tid] < max_tracking_count)
      TRACK_REGISTER[_name_tid]++;
   else
   {
      dum = true;
      return;
   }

   SetCurrent(this);
   start();
}

Profiling::Profiling(bool dummy)
   : dum(dummy)
   , func("DUMMY")
{}

Profiling::~Profiling()
{
   destruct();
}

void Profiling::start()
{
   begin = high_resolution_clock::now() - APPSTART;
   auto b = begin;
   QString f = QString::fromStdString(func);
   auto tid = std::hash<std::thread::id>{}(std::this_thread::get_id());
   ThreadManager::Insert([b, f, tid]() {
      Collector::WriteBegin({
         f,
         static_cast<std::size_t>(b.count()),
         0,
         tid
                            });
   });
}

void Profiling::stop()
{
   end = high_resolution_clock::now() - APPSTART;
   auto b = begin;
   auto e = end;
   QString f = QString::fromStdString(func);
   auto tid = std::hash<std::thread::id>{}(std::this_thread::get_id());

   ProfilingData data =
   {
         f,
         static_cast<std::size_t>(b.count()),
         static_cast<std::size_t>(e.count()),
         tid
   };

   ThreadManager::Insert([data]() {
      Collector::WriteEnd(data);
   });

   Analyzer::RegisterProfilingData(data);
}

void Profiling::save()
{
   std::cout << func << ": " << std::to_string(result() * std::pow(10, -6)) << " ms" << std::endl;
}

void Profiling::destruct()
{
   if (!enabled) return;
   if (m_deleted) return;
   if (dum) return;

   m_deleted = true;
   DelCurrent();
   stop();
   MUTEX.lock();
   MUTEX.unlock();
}

long long Profiling::result() const
{
   auto el = end - begin;
   return std::chrono::duration_cast<std::chrono::nanoseconds>(el).count();
}
