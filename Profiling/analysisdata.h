#pragma once

#include "stdafx.h"

struct AnalysisData
{
   QString
      function_name;

   std::size_t
      duration_min,
      duration_max;

   double
      period_mean,
      duration_mean,
      duration_stddev;

};