#pragma once

#include "stdafx.h"

#include <windows.h>
#include <excpt.h>

class TerminationHandler
{
public:

   enum TerminationType
   { EndOfProgram, UncaughtException, AccessViolation };

   static void RegisterAnyTerminationHandler(std::function<void()> func);
   static void RegisterEndOfProgramHandler(std::function<void()> func);
   static void RegisterUncaughtExceptionHandler(std::function<void()> func);
   static void RegisterAccessViolationHandler(std::function<void()> func);

   static void CallAnyTerminationHandler();
   static void CallEndOfProgramHandler();
   static void CallUncaughtExceptionHandler();
   static void CallAccessViolationHandler();

   static void ExecuteCurrentHandler();

   static TerminationType CurrentTerminationType;
   static int ExceptionFilter(unsigned int code, struct _EXCEPTION_POINTERS* ep);

   TerminationHandler() = default;
   TerminationHandler(const TerminationHandler&) = default;
   TerminationHandler(TerminationHandler&&) = default;
   ~TerminationHandler() = default;

   TerminationHandler(std::function<void()> handle);

   void call();

private:

   static std::unique_ptr<TerminationHandler> m_ath;
   static std::unique_ptr<TerminationHandler> m_eph;
   static std::unique_ptr<TerminationHandler> m_ueh;
   static std::unique_ptr<TerminationHandler> m_avh;

   std::function<void()> m_handle = nullptr;
};
