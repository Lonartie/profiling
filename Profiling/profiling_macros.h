#pragma once

#define PROFILING_LINE __LINE__
#define PROFILING_FUNC __FUNCTION__

#define PROFILING_STRING_IMPL(x) #x
#define PROFILING_STRING(x) PROFILING_STRING_IMPL(x)

#define PROFILING_CAT_IMPL(x, y) x##y
#define PROFILING_CAT(x, y) PROFILING_CAT_IMPL(x, y)

#define PROFILING_NAME PROFILING_CAT(__scope_profiler__, PROFILING_LINE)

/// @brief parameters has to be passed in order!
/// @note \p function_name is suffix for real function name
/// @param function_name      [opt] string
/// @param max_tracking_count [opt] std::size_t (may be 0!)
#define PROFILING(...) Profiling PROFILING_NAME ( PROFILING_FUNC "  " __VA_ARGS__ )
#define CURRENT_PROFILER (Profiling::GetCurrent())

/// @brief parameters has to be passed in order!
/// @note \p function_name is suffix for real function name
/// @param function_name      [opt] string
/// @param max_tracking_count [opt] std::size_t (may not be 0!)
#define PROFILING_BEGIN(...) Profiling PROFILING_NAME( PROFILING_FUNC "  " __VA_ARGS__ )
#define PROFILING_END() if(CURRENT_PROFILER) CURRENT_PROFILER->destruct();

#if defined(PROFILE)
#  define USE_PROFILING true
#else
#  define USE_PROFILING false
#endif

#define PROFILING_AUTO_ENABLED() auto __PROFILING_ENABLED__ = [](){ Profiling::SetEnabled(USE_PROFILING); return USE_PROFILING; }()
#define PROFILING_ENABLED() auto __PROFILING_ENABLED__ = [](){ Profiling::SetEnabled(true); return true; }()
#define PROFILING_DISABLED() auto __PROFILING_ENABLED__ = [](){ Profiling::SetEnabled(false); return false; }()

#define CALLER() [](){ return Profiling::GetModuleName(); } ()
#define CHECK_ADMIN() []() { if (Profiling::IsEnabled() && !Profiling::IsElevated()) { system(QString("admin.bat \"\"%1\"\"").arg(CALLER()).toLatin1().constData()); std::exit(0); } } ()
#define CALL_INDIRECT(call) [&]() call ()
#define MAIN_IMPL(call)                                                                                  \
{                                                                                                        \
   CHECK_ADMIN();                                                                                        \
   if (Profiling::IsEnabled()) Collector::Init();                                                        \
   if (Profiling::IsEnabled()) PerformanceCounter::Start(std::chrono::milliseconds(250));                \
   __try { CALL_INDIRECT(call); }                                                                        \
   __except (TerminationHandler::ExceptionFilter(GetExceptionCode(), GetExceptionInformation())) {}      \
   if (Profiling::IsEnabled()) TerminationHandler::ExecuteCurrentHandler();                              \
   if (Profiling::IsEnabled()) PerformanceCounter::Stop();                                               \
   if (Profiling::IsEnabled()) Collector::Close();                                                       \
   if (Profiling::IsEnabled()) CALL_INDIRECT({ProfilingArchiver::ZipFiles("./", "*.log", "dump.dump");});\
   return -1;                                                                                            \
}

#define MAIN(argc, argv) main (argc, argv) MAIN_IMPL
#define DEFINE_MAIN(func) int func (int argc, char**argv); int MAIN(int argc, char** argv) ({ return func (argc, argv); })
