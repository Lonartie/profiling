#include "stdafx.h"
#include "terminationhandler.h"


std::unique_ptr<TerminationHandler> TerminationHandler::m_ath = nullptr;
std::unique_ptr<TerminationHandler> TerminationHandler::m_eph = nullptr;
std::unique_ptr<TerminationHandler> TerminationHandler::m_ueh = nullptr;
std::unique_ptr<TerminationHandler> TerminationHandler::m_avh = nullptr;
TerminationHandler::TerminationType TerminationHandler::CurrentTerminationType = EndOfProgram;


void TerminationHandler::RegisterAnyTerminationHandler(std::function<void()> func)
{
   m_ath = std::make_unique<TerminationHandler>(func);
}

void TerminationHandler::RegisterEndOfProgramHandler(std::function<void()> func)
{
   m_eph = std::make_unique<TerminationHandler>(func);
}

void TerminationHandler::RegisterUncaughtExceptionHandler(std::function<void()> func)
{
   m_ueh = std::make_unique<TerminationHandler>(func);
}

void TerminationHandler::RegisterAccessViolationHandler(std::function<void()> func)
{
   m_avh = std::make_unique<TerminationHandler>(func);
}





void TerminationHandler::CallAnyTerminationHandler()
{
   if (m_ath) m_ath->call();
}

void TerminationHandler::CallEndOfProgramHandler()
{
   if (m_eph) m_eph->call();
}

void TerminationHandler::CallUncaughtExceptionHandler()
{
   if (m_ueh) m_ueh->call();
}

void TerminationHandler::CallAccessViolationHandler()
{
   if (m_avh) m_avh->call();
}






void TerminationHandler::ExecuteCurrentHandler()
{
   switch (CurrentTerminationType)
   {
   case EndOfProgram:
      CallEndOfProgramHandler();
      break;
   case UncaughtException:
      CallUncaughtExceptionHandler();
      break;
   case AccessViolation:
      CallAccessViolationHandler();
      break;
   }

   CallAnyTerminationHandler();
}






int TerminationHandler::ExceptionFilter(unsigned int code, struct _EXCEPTION_POINTERS* ep)
{
   if (code == EXCEPTION_ACCESS_VIOLATION)
   {
      CurrentTerminationType = AccessViolation;
   }
   else
   {
      CurrentTerminationType = UncaughtException;
   }

   return EXCEPTION_CONTINUE_EXECUTION;
}





TerminationHandler::TerminationHandler(std::function<void()> handle)
{
   m_handle = handle;
}

void TerminationHandler::call()
{
   if (m_handle) m_handle();
}

