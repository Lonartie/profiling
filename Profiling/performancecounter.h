#pragma once

#include "stdafx.h"
#include "../HardwareAdapter/ComputerObject.h"

#include <iostream>

class PerformanceCounter
{
public:

   template<typename base, intmax_t r1, intmax_t r2>
   static void Start(std::chrono::duration<base, std::ratio<r1, r2>> interval);
   static void Stop();

private:
   
   static std::map<HardwareAdapter::IHardwareObject, std::vector<HardwareAdapter::ISensorObject>> get_data();
   static std::chrono::duration<long long, std::nano> measure(std::function<void()> fn);

   static void work();
   static void write_begin();
   static void write_node();
   static void write_end();

   static HardwareAdapter::ComputerObject computer;
   static std::chrono::duration<long long, std::nano> period;
   static std::chrono::time_point<std::chrono::steady_clock> begin;
   static std::thread worker;
   static volatile bool stopped;
   static std::map<unsigned, QFile> files;

};

template<typename base, intmax_t r1, intmax_t r2>
void PerformanceCounter::Start(std::chrono::duration<base, std::ratio<r1, r2>> interval)
{
   period = std::chrono::duration_cast<decltype(period)>(interval);
   std::cout << "collecting hardware information for performance counters\n";
   computer.Open();
   stopped = false;
   write_begin();
   worker = std::thread(&work);
}
