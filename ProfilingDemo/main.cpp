#include <QtCore/QCoreApplication>
#include "../Profiling/profiling.h"
#include <functional>
#include <vector>
#include <iostream>

DEFINE_MAIN(_main);
PROFILING_ENABLED();

std::vector<std::function<void()>> funcs;
#define FUNC(RET, NAME, ...) RET NAME (__VA_ARGS__); auto func##__LINE__ = [&](){ funcs.push_back(NAME); return true; }(); RET NAME (__VA_ARGS__)

namespace nmspace
{
   struct strct
   {
      static void CalculateFib()
      {
         PROFILING();
         PROFILING_BEGIN("SETUP");
         std::size_t last = 1;
         std::size_t lastlast = 1;

         std::size_t iteration = 2;
         std::size_t current = 0;
         PROFILING_END();

         PROFILING_BEGIN("ITERATION");
         while (iteration++ < 1e6)
         {
            PROFILING("ONE ITERATION", 100);
            auto temp = current;
            current = last + lastlast;
            lastlast = last;
            last = current;
         }
         PROFILING_END();
      }
   };
}

FUNC(void, FIB_THREADED)
{
   PROFILING();
   std::vector<std::thread> threads;

   PROFILING_BEGIN("Create Threads");
   for (int i = 0; i < 100; i++)
   {
      threads.push_back(std::thread([]()
      {
         nmspace::strct::CalculateFib();
      }));
   }
   PROFILING_END();
   PROFILING_BEGIN("Wait for Threads");
   for (auto& th : threads)
   {
      static int id = 0;
      std::cout << "waiting for: " << id++ << std::endl;
      th.join();
   }
   PROFILING_END();
}

#define ENABLED true
int _main(int argc, char** argv)
{
   QCoreApplication app(argc, argv);

   if (ENABLED)
      for (const auto& func : funcs)
         func();

   //std::this_thread::sleep_for(std::chrono::minutes(5));

   return 0;
}