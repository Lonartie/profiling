#pragma once

#define ENUM_TO_STRING_MAP(name) case name: return #name
#define STRING_TO_ENUM_MAP(input, name) if (input == #name) return name

namespace HardwareAdapter
{
   enum HardwareTypeObject
   {
      Mainboard,
      SuperIO,
      CPU,
      RAM,
      GpuNvidia,
      GpuAti,
      TBalancer,
      Heatmaster,
      HDD,
      ERROR,
   };

   static QString HardwareTypeToString(HardwareTypeObject o)
   {
      switch(o)
      {
         ENUM_TO_STRING_MAP(Mainboard   );
         ENUM_TO_STRING_MAP(SuperIO     );
         ENUM_TO_STRING_MAP(CPU         );
         ENUM_TO_STRING_MAP(RAM         );
         ENUM_TO_STRING_MAP(GpuNvidia   );
         ENUM_TO_STRING_MAP(GpuAti      );
         ENUM_TO_STRING_MAP(TBalancer   );
         ENUM_TO_STRING_MAP(Heatmaster  );
         ENUM_TO_STRING_MAP(HDD         );
      default: return "Error";
      }
   }

   static HardwareTypeObject StringToHardwareType(const QString& o)
   {
      STRING_TO_ENUM_MAP(o, Mainboard   );
      STRING_TO_ENUM_MAP(o, SuperIO     );
      STRING_TO_ENUM_MAP(o, CPU         );
      STRING_TO_ENUM_MAP(o, RAM         );
      STRING_TO_ENUM_MAP(o, GpuNvidia   );
      STRING_TO_ENUM_MAP(o, GpuAti      );
      STRING_TO_ENUM_MAP(o, TBalancer   );
      STRING_TO_ENUM_MAP(o, Heatmaster  );
      STRING_TO_ENUM_MAP(o, HDD         );
      return ERROR;
   }
}