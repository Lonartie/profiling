#pragma once
#include "dll.h"

namespace HardwareAdapter
{
   class HARDWAREADAPTER_EXPORT Zipper
   {
   public:

      static void ZipFolder(const QString& folderPath, const QString& outFile);
   };
}