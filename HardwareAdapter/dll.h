#pragma once

#if defined(HARDWAREADAPTER_EXPORT)
#  undef HARDWAREADAPTER_EXPORT
#  define HARDWAREADAPTER_EXPORT __declspec(dllexport)
#else
#  define HARDWAREADAPTER_EXPORT __declspec(dllimport)
#endif