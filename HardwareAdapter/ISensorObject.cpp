#include "pch.h"
#include "ISensorObject.h"
#include "CliHelper.h"

using namespace System;
using namespace OpenHardwareMonitor;

namespace
{
   auto FromPointer = &HardwareAdapter::CliFromPointer<Hardware::ISensor^>;
}

HardwareAdapter::ISensorObject::ISensorObject()
   : m_sensor(nullptr) {}

HardwareAdapter::ISensorObject::ISensorObject(void* data)
   : m_sensor(data) {}

bool HardwareAdapter::ISensorObject::IsValid() const
{
   return m_sensor;
}

HardwareAdapter::SensorTypeObject HardwareAdapter::ISensorObject::GetSensorType() const
{
   return static_cast<SensorTypeObject>(FromPointer(m_sensor)->SensorType);
}

QString HardwareAdapter::ISensorObject::GetName() const
{
   return CliToQString(FromPointer(m_sensor)->Name);
}

float HardwareAdapter::ISensorObject::GetValue() const
{
   return FromPointer(m_sensor)->Value.GetValueOrDefault();
}

std::vector<HardwareAdapter::IParameterObject> HardwareAdapter::ISensorObject::GetParameters() const
{
   std::vector<IParameterObject> result;
   auto list = FromPointer(m_sensor)->Parameters;

   for (auto i = 0; i < list->Length; i++)
      result.emplace_back<IParameterObject>(CliToPointer(list[i]));

   return result;
}
