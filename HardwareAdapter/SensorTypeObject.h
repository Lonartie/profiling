#pragma once

#define ENUM_TO_STRING_MAP(name) case name: return #name
#define STRING_TO_ENUM_MAP(input, name) if (input == #name) return name

namespace HardwareAdapter
{
   enum SensorTypeObject
   {
      Voltage,
      Clock,
      Temperature,
      Load,
      Fan,
      Flow,
      Control,
      Level,
      Factor,
      Power,
      Data,
      SmallData,
      Error
   };

   static QString SensorTypeToString(SensorTypeObject o)
   {
      switch(o)
      {
         ENUM_TO_STRING_MAP(Voltage       );
         ENUM_TO_STRING_MAP(Clock         );
         ENUM_TO_STRING_MAP(Temperature   );
         ENUM_TO_STRING_MAP(Load          );
         ENUM_TO_STRING_MAP(Fan           );
         ENUM_TO_STRING_MAP(Flow          );
         ENUM_TO_STRING_MAP(Control       );
         ENUM_TO_STRING_MAP(Level         );
         ENUM_TO_STRING_MAP(Factor        );
         ENUM_TO_STRING_MAP(Power         );
         ENUM_TO_STRING_MAP(Data          );
         ENUM_TO_STRING_MAP(SmallData     );
      default: return "Error";
      }
   }

   static SensorTypeObject StringToSensorType(const QString& o)
   {
      STRING_TO_ENUM_MAP(o, Voltage       );
      STRING_TO_ENUM_MAP(o, Clock         );
      STRING_TO_ENUM_MAP(o, Temperature   );
      STRING_TO_ENUM_MAP(o, Load          );
      STRING_TO_ENUM_MAP(o, Fan           );
      STRING_TO_ENUM_MAP(o, Flow          );
      STRING_TO_ENUM_MAP(o, Control       );
      STRING_TO_ENUM_MAP(o, Level         );
      STRING_TO_ENUM_MAP(o, Factor        );
      STRING_TO_ENUM_MAP(o, Power         );
      STRING_TO_ENUM_MAP(o, Data          );
      STRING_TO_ENUM_MAP(o, SmallData     );
      return Error;
   }
}