#pragma once

#include "pch.h"
#include "dll.h"
#include "IHardwareObject.h"


namespace HardwareAdapter
{
	class HARDWAREADAPTER_EXPORT ComputerObject
	{
	public:

      ComputerObject();
      ComputerObject(void* data);
      ~ComputerObject();

      bool IsValid() const;

      void Open() const;
      std::vector<IHardwareObject> GetHardware() const;

	private:

      void* m_computer = nullptr;
	};
}
