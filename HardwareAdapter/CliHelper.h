#pragma once

#include "pch.h"
#include <msclr\marshal_cppstd.h>

namespace HardwareAdapter
{
   static void* CliToPointer(System::Object^ obj)
   {
      auto handle = System::Runtime::InteropServices::GCHandle::Alloc(obj);
      auto ptr = System::Runtime::InteropServices::GCHandle::ToIntPtr(handle);
      return ptr.ToPointer();
   }


   template<typename T = System::Object^>
   static T CliFromPointer(void* data)
   {
      auto ptr = System::IntPtr(data);
      auto handle = System::Runtime::InteropServices::GCHandle::FromIntPtr(ptr);
      return safe_cast<T>(handle.Target);
   }


   static void CliFreePointer(void* data)
   {
      auto ptr = System::IntPtr(data);
      auto handle = System::Runtime::InteropServices::GCHandle::FromIntPtr(ptr);
      handle.Free();
   }


   static QString CliToQString(System::String^ string)
   {
      if (string == nullptr) return {};
      msclr::interop::marshal_context context;
      return QString::fromStdString(context.marshal_as<std::string>(string));      
   }


   static System::String^ CliFromQString(const QString& string)
   {
      msclr::interop::marshal_context context;
      return context.marshal_as<System::String^>(string.toLatin1().constData());
   }
}