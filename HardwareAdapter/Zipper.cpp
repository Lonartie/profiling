#include "pch.h"
#include "Zipper.h"
#include "CliHelper.h"

using namespace System::IO::Compression;

void HardwareAdapter::Zipper::ZipFolder(const QString& folderPath, const QString& outFile)
{
   ZipFile::CreateFromDirectory(
      CliFromQString(folderPath), 
      CliFromQString(outFile),
      CompressionLevel::Optimal, 
      false);
}
