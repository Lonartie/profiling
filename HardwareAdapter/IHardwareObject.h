#pragma once

#include "pch.h"
#include "dll.h"
#include "HardwareTypeObject.h"
#include "ISensorObject.h"

namespace HardwareAdapter
{
   class HARDWAREADAPTER_EXPORT IHardwareObject
   {
   public:

      IHardwareObject();
      IHardwareObject(void* data);
      IHardwareObject(const IHardwareObject& o);
      bool IsValid() const;

      void Update() const;
      HardwareTypeObject GetHardwareType() const;
      QString GetReport() const;
      std::vector<ISensorObject> GetSensors() const;
      std::vector<IHardwareObject> GetSubHardware() const;
      QString GetName() const;

      bool operator==(const IHardwareObject& o) const noexcept
      { return m_hardware == o.m_hardware; }

      bool operator!=(const IHardwareObject & o) const noexcept
      { return m_hardware != o.m_hardware; }

      bool operator<(const IHardwareObject& o) const noexcept
      { return m_hardware < o.m_hardware; }

      bool operator>(const IHardwareObject & o) const noexcept
      { return m_hardware > o.m_hardware; }

      bool operator<=(const IHardwareObject& o) const noexcept
      { return m_hardware <= o.m_hardware; }

      bool operator>=(const IHardwareObject& o) const noexcept
      { return m_hardware >= o.m_hardware; }

   private:

      void* m_hardware = nullptr;
   };
}