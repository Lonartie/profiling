#pragma once

#include "pch.h"
#include "dll.h"
#include "SensorTypeObject.h"
#include "IParameterObject.h"

namespace HardwareAdapter
{
   class HARDWAREADAPTER_EXPORT ISensorObject
   {
   public:

      ISensorObject();
      ISensorObject(void* data);
      bool IsValid() const;

      SensorTypeObject GetSensorType() const;
      QString GetName() const;
      float GetValue() const;
      std::vector<IParameterObject> GetParameters() const;

      bool operator==(const ISensorObject& o) const noexcept
      { return m_sensor == o.m_sensor; }

      bool operator!=(const ISensorObject& o) const noexcept
      { return m_sensor != o.m_sensor; }

      bool operator<(const ISensorObject& o) const noexcept
      { return m_sensor < o.m_sensor; }

      bool operator>(const ISensorObject& o) const noexcept
      { return m_sensor > o.m_sensor; }

      bool operator<=(const ISensorObject& o) const noexcept
      { return m_sensor <= o.m_sensor; }

      bool operator>=(const ISensorObject& o) const noexcept
      { return m_sensor >= o.m_sensor; }

   private:

      void* m_sensor = nullptr;
   };
}