#include "pch.h"
#include "ComputerObject.h"
#include "CliHelper.h"

using namespace System;
using namespace OpenHardwareMonitor;
using namespace System::Runtime::InteropServices;

namespace
{
   auto FromPointer = &HardwareAdapter::CliFromPointer<Hardware::Computer^>;
}

HardwareAdapter::ComputerObject::ComputerObject()
   : m_computer(CliToPointer(gcnew Hardware::Computer())) {}

HardwareAdapter::ComputerObject::ComputerObject(void* data)
   : m_computer(data) {}

HardwareAdapter::ComputerObject::~ComputerObject()
{
   CliFreePointer(m_computer);
}

bool HardwareAdapter::ComputerObject::IsValid() const
{
   return m_computer != nullptr;
}

void HardwareAdapter::ComputerObject::Open() const
{
   FromPointer(m_computer)->Open();
   FromPointer(m_computer)->CPUEnabled = true;
   FromPointer(m_computer)->RAMEnabled = true;
   FromPointer(m_computer)->GPUEnabled = true;
   FromPointer(m_computer)->HDDEnabled = true;
   FromPointer(m_computer)->FanControllerEnabled = true;
   FromPointer(m_computer)->MainboardEnabled = true;
}

std::vector<HardwareAdapter::IHardwareObject> HardwareAdapter::ComputerObject::GetHardware() const
{
   std::vector<IHardwareObject> result;
   auto list = FromPointer(m_computer)->Hardware;

   for (auto i = 0; i < list->Length; i++)
      result.emplace_back<IHardwareObject>(CliToPointer(list[i]));

   return result;
}
