#include "pch.h"
#include "IParameterObject.h"
#include "CliHelper.h"

using namespace OpenHardwareMonitor;

namespace
{
   auto FromPointer = &HardwareAdapter::CliFromPointer<Hardware::IParameter^>;
}

HardwareAdapter::IParameterObject::IParameterObject()
   : m_parameter(nullptr) {}

HardwareAdapter::IParameterObject::IParameterObject(void* data)
   : m_parameter(data) {}


bool HardwareAdapter::IParameterObject::IsValid() const
{
   return m_parameter;
}

QString HardwareAdapter::IParameterObject::GetName() const
{
   return CliToQString(FromPointer(m_parameter)->Name);
}

float HardwareAdapter::IParameterObject::GetValue() const
{
   return FromPointer(m_parameter)->Value;
}