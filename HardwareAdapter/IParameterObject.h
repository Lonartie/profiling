#pragma once

#include "pch.h"
#include "dll.h"

namespace HardwareAdapter
{
   class HARDWAREADAPTER_EXPORT IParameterObject
   {
   public:

      IParameterObject();
      IParameterObject(void* data);
      bool IsValid() const;

      QString GetName() const;
      float GetValue() const;

   private:

      void* m_parameter = nullptr;
   };
}