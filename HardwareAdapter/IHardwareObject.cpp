#include "pch.h"
#include "IHardwareObject.h"
#include "CliHelper.h"

using namespace OpenHardwareMonitor;

namespace
{
   auto FromPointer = &HardwareAdapter::CliFromPointer<Hardware::IHardware^>;
}

HardwareAdapter::IHardwareObject::IHardwareObject()
   : m_hardware(nullptr) {}

HardwareAdapter::IHardwareObject::IHardwareObject(const IHardwareObject& o)
   : m_hardware(o.m_hardware) {}

HardwareAdapter::IHardwareObject::IHardwareObject(void* data)
   : m_hardware(data) {}

bool HardwareAdapter::IHardwareObject::IsValid() const
{
   return m_hardware != nullptr;
}

void HardwareAdapter::IHardwareObject::Update() const
{
   FromPointer(m_hardware)->Update();
}

HardwareAdapter::HardwareTypeObject HardwareAdapter::IHardwareObject::GetHardwareType() const
{
   return static_cast<HardwareTypeObject>(FromPointer(m_hardware)->HardwareType);
}

QString HardwareAdapter::IHardwareObject::GetReport() const
{
   return CliToQString(FromPointer(m_hardware)->GetReport());
}

std::vector<HardwareAdapter::ISensorObject> HardwareAdapter::IHardwareObject::GetSensors() const
{
   std::vector<ISensorObject> result;
   auto list = FromPointer(m_hardware)->Sensors;

   for (auto i = 0; i < list->Length; i++)
      result.emplace_back<ISensorObject>(CliToPointer(list[i]));

   return result;
}

std::vector<HardwareAdapter::IHardwareObject> HardwareAdapter::IHardwareObject::GetSubHardware() const
{
   std::vector<IHardwareObject> result;
   auto list = FromPointer(m_hardware)->SubHardware;

   for (auto i = 0; i < list->Length; i++)
      result.emplace_back<IHardwareObject>(CliToPointer(list[i]));

   return result;
}

QString HardwareAdapter::IHardwareObject::GetName() const
{
   return CliToQString(FromPointer(m_hardware)->Name);
}
